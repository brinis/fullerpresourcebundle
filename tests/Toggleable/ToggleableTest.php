<?php

namespace FullErp\ResourceBundle\Tests\Toggleable;

use FullErp\ResourceBundle\Tests\Toggleable\Fixture\UsingTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 08/06/2022
 * Time: 17:11.
 */
class ToggleableTest extends TestCase
{
    public function testGetSetDeletedAt(): void
    {
        /**
         * @var $entity UsingTrait
         */
        $entity = new UsingTrait();
        $this->assertTrue($entity->isEnabled(), 'isEnabled defaults to true');
        $this->assertSame($entity, $entity->disable(), 'Setting enabled to false undeletes object');
        $this->assertFalse($entity->isEnabled(), 'isEnabled is false when enabled is  equal to false');
        $this->assertSame($entity, $entity->enable(), 'Setting enabled to true undeletes object');
        $this->assertTrue($entity->isEnabled(), 'isEnabled is true when enabled is  equal to true');
    }
}
